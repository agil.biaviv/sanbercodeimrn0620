let createArray = (fromNumber, toNumber, step = 1) => {
    //cek angka
    fromNumber = parseInt(fromNumber)
    toNumber = parseInt(toNumber)    
    if(isNaN(fromNumber) || isNaN(toNumber)) {
        return -1
    } 
    // ========
    var tempArray = []
    var arrayLength = Math.floor(Math.abs((fromNumber - toNumber) / step))

    if( fromNumber > toNumber ) step = -step // for descending

    for(i = 0; i <= arrayLength; i++) {
        tempArray[i] = fromNumber + (step * i)
    }

    return tempArray
}

let range = (num1, num2) => {
   return createArray(num1, num2) // waste of resource
}

console.log(`\nNo. 1`)
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range())
console.log(`===========================================\n`)
// ===================================
let rangeWithStep = (startNum, finishNum, step) => { 
    return createArray(startNum, finishNum, step)
} 

console.log(`\nNo. 2`)
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log(`===========================================\n`)

let sum = (startNum, finishNum, step = 1) => {
    var tempArray = createArray(startNum, finishNum, step)
    if (Array.isArray(tempArray)) { // cek jika variabel tempArray adalah Array
        return tempArray.reduce((totalPrevNumber, nextNumber) => totalPrevNumber + nextNumber)
    }
    return 0
}
console.log(`\nNo. 3`)
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
console.log(`===========================================\n`)

//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

let dataHandling = (dataArray) => {
    var temp = ''
    for(i = 0; i < dataArray.length; i++) {
        temp+=`Nomor ID : ${dataArray[i][0]}\nNama Lengkap : ${dataArray[i][1]}\nTTL : ${dataArray[i][2]}, ${dataArray[i][3]}\nHobi : ${dataArray[i][4]}\n\n`
    }
    return temp
}

console.log(`\nNo. 4`)
console.log(dataHandling(input))
console.log(`===========================================\n`)
let balikKata = kata => {
    var temp = ''
    for(i = kata.length; i >= 0; i--) {
        temp+= kata.charAt(i)
    }
    return temp
}

console.log(`\nNo. 5`)
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log(`===========================================\n`)

var input2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]

let dataHandling2 = (data) => {
    var [...temp] = data //
    var [,nama, provinsi] = temp
    nama += " Elsharawy"
    provinsi = `Provinsi ${provinsi}`

    temp.splice(1, 2, nama, provinsi)
    temp.splice(4, 1, "Pria", "SMA Internasional Metro")

    console.log(temp)
    // console.log(data)
    var tanggal = temp[3].split('/')
    var bulan = tanggal[1]
    var namaBulan = cekNamaBulan(bulan)

    var [...tanggalDesc] = tanggal //
    tanggalDesc = tanggalDesc.sort((a,b) => b - a ) 

    console.log(namaBulan)
    console.log(tanggalDesc)
    console.log(tanggal.join('-'))//
    console.log(data[1]) // 
}

let cekNamaBulan = bulan => {
    var namaBulan
    switch(parseInt(bulan)) {
        case 1: 
            namaBulan = 'Januari'
            break
        case 2: 
            namaBulan = 'Februari'
            break
        case 3:
            namaBulan = 'Maret'
            break
        case 4: 
            namaBulan = 'April'
            break
        case 5:
            namaBulan = 'Mei'
            break
        case 6: 
            namaBulan = 'Juni'
            break
        case 7:
            namaBulan = 'Juli'
            break
        case 8: 
            namaBulan = 'Agustus'
            break
        case 9:
            namaBulan = 'September'
            break
        case 10: 
            namaBulan = 'Oktober'
            break
        case 11:
            namaBulan = 'November'
            break
        case 12: 
            namaBulan = 'Desember'
            break
        default:
            namaBulan = 'Invalid'
            break
    }

    return namaBulan
}
console.log(`\nNo. 6`)
dataHandling2(input2)
console.log(`===========================================\n`)
