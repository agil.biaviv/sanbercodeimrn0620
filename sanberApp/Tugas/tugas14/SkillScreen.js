import React from 'react';
import { View, SafeAreaView, StyleSheet, Image, Text, TouchableOpacity, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import SkillList from './components/SkillList'
import data from './skillData.json'

const SkillScreenPage = (props) => {
  return ( 
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <Image source={require('./images/logo.png')} style={{width: 200, height: 70}} />
      </View>
      <View style={styles.userProfile}>
        <Icon name="account-circle" style={{color: secondaryColor}} size={30} />
        <View style={styles.userName}>
          <Text style={{fontSize: 12}}>Hai,</Text>
          <Text style={{color: primaryColor}}>Agil Bi Aviv Taufiqi</Text>
        </View>

      </View>

      <Text style={{...styles.headings, borderBottomWidth: 2, borderBottomColor: secondaryColor}}>SKILL</Text>

      <View style={styles.filters}>
        <TouchableOpacity style={styles.filterItem}>
          <Text style={{color: primaryColor, fontSize: 12, fontWeight: "bold"}}>Library / Framework</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.filterItem}>
          <Text style={{color: primaryColor, fontSize: 12, fontWeight: "bold"}}>Bahasa Pemrograman</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.filterItem}>
          <Text style={{color: primaryColor, fontSize: 12, fontWeight: "bold"}}>Teknologi</Text>
        </TouchableOpacity>
      </View>

        <FlatList 
          data={data.items}
          renderItem={(skill) => <SkillList skill={skill.item} /> }
          keyExtractor={(data) => data.id.toString() } 
        />
    </SafeAreaView>
  );
}

const primaryColor = "#003366"
const secondaryColor = "#3EC6FF"

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15
  },
  header: {
    marginLeft: "auto"
  },
  userProfile: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 5
  },
  userName: {
    marginLeft: 10
  },
  headings: {
    color: primaryColor, 
    fontSize: 26, 
    paddingVertical: 5
  },
  filters : {
    flexDirection: "row",
    justifyContent: "space-around",
    marginVertical: 10
  },
  filterItem : {
    backgroundColor: secondaryColor,
    borderRadius: 5,
    paddingVertical: 2,
    paddingHorizontal: 6,
  }
})
 
export default SkillScreenPage;