import React from 'react';
import LoginPage from './views/LoginPage';
import RegisterPage from './views/RegisterPage';
import AboutPage from './views/AboutUs';
import { StatusBar, ScrollView } from 'react-native';

const Tugas13 = () => {
  return ( 
    <>
      <StatusBar translucent={false} backgroundColor="whitesmoke" barStyle="dark-content" />
      <ScrollView>
        <LoginPage />
        <RegisterPage />
        <AboutPage />
      </ScrollView>
    </>
  );
}
 
export default Tugas13;