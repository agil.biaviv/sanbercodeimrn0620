import React from 'react';
import { View, StyleSheet, Image, Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


const AboutPage = () => {
  return ( 
    <View style={style.container}>

      <View>
        <Text style={{color: "#003366", fontSize: 20, textAlign: "center", fontWeight: "bold", marginBottom: 10, }}>Tentang Saya</Text>
        <Icon name="account" size={150} style={{padding: 10, borderRadius: 100, backgroundColor: "whitesmoke", color:  "#ddd"}}/>
      </View>

      <View style={{ height: 100, justifyContent: "center", alignItems: "center" }}>
        <Text style={{color: "#003366", fontSize: 16, fontWeight: "700"}}>Agil Bi Aviv Taufiqi</Text>
        <Text style={{color: "#3EC6FF", fontSize: 16, fontWeight: "500"}}>React Native Developer</Text>
      </View>

      <View style={style.card}>
        <Text style={{...style.cardTitle, ...style.textPrimary, borderColor: "#003366"}}>Portofolio</Text>
        <View style={style.cardBody}>
          <View style={style.horizontalList}>
            <Icon name="gitlab" size={40} style={{color: "#3EC6FF"}}/>
            <Text style={{marginTop: 10, ...style.textPrimary, }}>@Agil.biaviv</Text>
          </View>

          <View style={style.horizontalList}>
            <Icon name="github-circle" size={40} style={{color: "#3EC6FF"}}/>
            <Text style={{marginTop: 10, ...style.textPrimary, }}>@Agilbiavivt</Text>
          </View>

        </View>
      </View>
      <View style={style.card}>
        <Text style={{...style.cardTitle, ...style.textPrimary, borderColor: "#003366"}}>Hubungi Saya</Text>
        <View style={{...style.cardBody, flexDirection: "column", paddingHorizontal: 80, paddingVertical: 5}}>

          <View style={style.verticalList}>
            <Icon name="facebook-box" size={40} style={{color: "#3EC6FF"}}/>
            <Text style={{...style.textPrimary, width: 120, }}>Agil Bi Aviv Taufiqi</Text>
          </View>
          <View style={style.verticalList}>
            <Icon name="instagram" size={40} style={{color: "#3EC6FF"}}/>
            <Text style={{marginLeft: 20, ...style.textPrimary, width: 120, }}>@Agilbiavivt</Text>
          </View>
          <View style={style.verticalList}>
            <Icon name="twitter" size={40} style={{color: "#3EC6FF"}}/>
            <Text style={{marginLeft: 20, ...style.textPrimary, width: 120, }}>@abcdefghi</Text>
          </View>
        </View>
      </View>
      
    </View>
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    paddingTop: 40
  },
  profileImage: {
    width: 130, 
    height: 130, 
    borderRadius: 100, 
    marginVertical: 10
  },
  card: {
    margin: 10,
    borderRadius: 10,
    minHeight: 100,
    backgroundColor: "whitesmoke",
    alignSelf: "stretch"
  },
  cardTitle: {
    color: "black",
    paddingVertical: 4,
    borderBottomWidth: .5,
    borderColor: "#555",
    textAlign: "center",
    fontSize: 14,
    marginHorizontal: 5,
  },
  cardBody: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: "space-evenly",
    alignItems: "center"
  },
  textPrimary: {
    color: '#003366'
  },
  verticalList: {
    alignSelf: "stretch", flexDirection: 'row', alignItems: "center", justifyContent: "space-between", marginVertical: 5
  },
  horizontalList: {
    justifyContent: "center", alignItems: "center", marginVertical: 10
  }
})
 
export default AboutPage;