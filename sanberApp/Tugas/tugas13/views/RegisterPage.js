import React from 'react';
import { View, StyleSheet, TouchableOpacity, Text, TextInput, Button, TouchableWithoutFeedback, Image, Keyboard, KeyboardAvoidingView, ScrollView} from 'react-native';

const RegisterPage = () => {
  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.logo}>
            <Image source={require('../images/logo.png')} style={{width: 300, height: 100}} />
          </View>
          <Text style={{textAlign: "center", fontSize: 20, fontWeight: "600", color: "#003366", marginBottom: 25, marginTop: 10}}>Register</Text>
          <View style={styles.loginBox}>
            <View style={{marginBottom: 10}}>
              <Text style={{color: "#003366"}}>Username</Text>
              <TextInput selectionColor="#003366" style={styles.input} />
            </View>
            <View style={{marginBottom: 10}}>
              <Text style={{color: "#003366"}}>Email</Text>
              <TextInput selectionColor="#003366" style={styles.input} />
            </View>
            <View style={{marginBottom: 10}}>
              <Text style={{color: "#003366"}}>Password</Text>
              <TextInput selectionColor="#003366" style={styles.input} />
            </View>
            <View style={{marginBottom: 10}}>
              <Text style={{color: "#003366"}}>Ulangi Password</Text>
              <TextInput selectionColor="#003366" style={styles.input} />
            </View>

            <View style={{marginTop: 10}}>
              <TouchableOpacity style={{...styles.button, backgroundColor: "#003366"}}>
                <Text style={styles.buttonText}>Daftar</Text>
              </TouchableOpacity>
              
              <Text style={{marginVertical: 5, textAlign: "center", fontSize: 18, color: "#3EC6FF"}}>atau</Text>
              
              <TouchableOpacity style={{...styles.button, backgroundColor: "#3EC6FF"}}>
                <Text style={styles.buttonText}>Masuk ?</Text>
              </TouchableOpacity>
            </View>

          </View>
        </View>
      </ScrollView>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 40,
    paddingVertical: 20
  },
  button: {
    borderRadius: 100,
    elevation: 1,
    alignSelf: "center",
  },
  buttonText: {
    paddingHorizontal: 30,
    paddingVertical: 10,
    fontSize: 16,
    color: "#fff",
  },
  input: {
    paddingHorizontal: 12, 
    height: 40, 
    borderWidth: 1, 
    borderColor: "#003366",
    color: "#003366"
  },
  loginBox: {
    flex: 1,
    alignSelf: "stretch"
  },
  logo: {
    marginBottom: 20,
  }
})

export default RegisterPage;