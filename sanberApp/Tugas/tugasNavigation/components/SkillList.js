import React from 'react';
import { View, TouchableOpacity, StyleSheet, Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const SkillList = (props) => {
  const skill = props.skill

  let icon = skill.iconName
  let skillName = skill.skillName
  let percentage = skill.percentageProgress
  let categoryName = skill.categoryName
  return ( 
    <TouchableOpacity style={styles.card}>
      <Icon name={icon} size={80} style={{color: primaryColor}} />
      <View style={styles.skillDesc}>
        <Text style={{color: primaryColor, fontWeight: "bold" ,fontSize: 18, alignSelf: "stretch"}}>{skillName}</Text>
        <Text style={{color: secondaryColor, alignSelf: "stretch"}}>{categoryName}</Text>
        <Text style={{color: "white", fontSize: 40, fontWeight: "bold", textAlign: "right"}}>{percentage}</Text>
      </View>
      <Icon name="chevron-right" size={80} style={{color: primaryColor, marginRight: -20}} />
    </TouchableOpacity>
  );
}

const primaryColor = "#003366"
const secondaryColor = "#3EC6FF"

const styles = StyleSheet.create({
  card: {
    backgroundColor: "#B4E9FF",
    borderRadius: 5,
    paddingHorizontal: 5,
    paddingVertical: 13,
    flexDirection: "row",
    alignItems: "center",
    overflow: "hidden",
    elevation: 5,
    marginBottom: 5
  },
  skillDesc: {
    flex: 1,
    paddingHorizontal: 10,
    justifyContent: "flex-end"
  }
})
 
export default SkillList;