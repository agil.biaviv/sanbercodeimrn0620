import React from 'react';
import { View, StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import LoginPage from './views/LoginPage'
import AboutPage from './views/AboutUs'
import ProjectPage from './views/ProjectScreen'
import SkillPage from './views/SkillScreen'
import AddScreenPage from './views/AddScreen'

const Tab = createBottomTabNavigator();
const TabStack = () => (
  <Tab.Navigator initialRouteName="Skill">
    <Tab.Screen name="Project" 
      component={ProjectStack} 
      options={{title: "Halaman Proyek"}}
    />
    <Tab.Screen 
      name="Skill" 
      component={SkillScreenStack} 
      options={{title: "Skill Screen"}}
    />
    <Tab.Screen 
      name="Add" 
      component={AddStack} 
      options={{title: "Halaman Tambah"}}
    />
  </Tab.Navigator>
)

const Project = createStackNavigator();
const ProjectStack = () => (
  <Project.Navigator>
    <Project.Screen name="Project" component={ProjectPage} />
  </Project.Navigator>
)

const SkillScreen = createStackNavigator();
const SkillScreenStack = () => (
  <SkillScreen.Navigator>
    <SkillScreen.Screen name="Skill" component={SkillPage} options={{ title: "Skill Screen" }} />
  </SkillScreen.Navigator>
)

const Add = createStackNavigator();
const AddStack = () => (
  <Add.Navigator>
    <Add.Screen name="add" component={AddScreenPage} options={{ title: "Add Screen" }} />
  </Add.Navigator>
)

const Drawer = createDrawerNavigator();
const HomeStack = () => (
  <Drawer.Navigator>
    <Drawer.Screen name="Home" component={TabStack} />
    <Drawer.Screen 
      name="About" 
      component={AboutPage} 
      options={{title: "Tentang Saya"}}
    />
  </Drawer.Navigator>
)


const Root = createStackNavigator();

const tugas15Navigation = () => {
  return ( 
    <NavigationContainer>
      <Root.Navigator>
        <Root.Screen name="Login" component={LoginPage} />
        <Root.Screen name="Home" component={HomeStack}/>
      </Root.Navigator>
      {/* <HomeStack /> */}
        {/* drawer */}
    </NavigationContainer>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  }
})
 
export default tugas15Navigation;