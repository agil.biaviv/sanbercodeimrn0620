import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

const AddScreen = () => {
  return ( 
    <View style={styles.container}>
      <Text>Halaman Tambah</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
})
 
export default AddScreen;