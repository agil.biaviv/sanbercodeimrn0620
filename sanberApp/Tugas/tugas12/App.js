import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from './components/VideoItem';
import data from './data.json';

const App = () => {
  return ( 
    <View style={styles.container}>
      <View style={styles.navBar}>
        <Image source={require('./images/logo.png')} style={{width: 94, height: 21}} />
        <View style={styles.rightNav}>
          <TouchableOpacity>
            <Icon style={styles.rightNavItem} name="search" size={25} />
          </TouchableOpacity>
          <TouchableOpacity>
            <Icon style={styles.rightNavItem} name="account-circle" size={25} />
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.body}>
        
        <FlatList 
          data={data.items}
          renderItem={(video) =>  <VideoItem video={video.item} videoIndex={video.index}/> }
          keyExtractor={(item) => item.id}
        />
      </View>

      <View style={styles.menuBar}>
        <TouchableOpacity style={styles.menuItem}>
          <Icon name="home" size={25} style={{color: "#888"}, styles.menuActive} />
          <Text style={{...styles.menuTitle, ...styles.titleActive}}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menuItem}>
          <Icon name="whatshot" size={25} style={{color: "#888"}} />
          <Text style={styles.menuTitle}>Trending</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menuItem}>
          <Icon name="subscriptions" size={25} style={{color: "#888"}} />
          <Text style={styles.menuTitle}>Subscriptions</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menuItem}>
          <Icon name="folder" size={25} style={{color: "#888"}} />
          <Text style={styles.menuTitle}>Library</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  navBar: {
    height: 55,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    alignItems: "center",
    flexDirection: 'row',
    justifyContent: "space-between"
  },
  rightNav: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  rightNavItem: {
    color: '#4c4c4c',
    marginLeft: 22
  },
  body: {
    flex: 1,
  }, 
  menuBar : {
    flexDirection: "row",
    height: 60,
    borderTopWidth: .5,
    borderColor: '#e5e5e5',
    paddingHorizontal: 15,
    alignItems: "center",
    justifyContent: "space-around"
  },
  menuItem : {
    justifyContent: "center",
    alignItems: "center"
  },
  menuActive: {
    color: "red"
  },
  menuTitle : {
    fontSize: 11,
    color: "#888"
  },
  titleActive: {
    color: "red"
  }
});

 
export default App;