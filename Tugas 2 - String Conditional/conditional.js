const readline = require('readline')

const readLineInt = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

let questions = question => {
    return new Promise((resolve) => {
        readLineInt.question(question, answer => {
            resolve(answer)
        })
    })
}

// ============================================= 

async function initWereWolf() {
    console.log(`Tugas 2 - Conditional (if else) `)
    console.log(`===========================================\n`)

    console.log(`Isi pertanyaan ini untuk memulai game werewolf!`)

    const nama =  await questions('Masukkan Namamu : ')
    const peran = await questions('peran : (penyihir, guard, werewolf)\n')

    if(nama != '') {
        if(peran == '') {
            console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`)
        } else {
            startTheGame(nama, peran)
        }
    } else { 
        console.log(`Masukkan namamu!`)
    }
    readLineInt.pause()
    printTanggal()
}

let startTheGame = (nama, peran) => {
    const penyihir = "kamu dapat melihat siapa yang menjadi werewolf!"
    const guard = "kamu akan membantu melindungi temanmu dari serangan werewolf."
    const wereWolf = "Kamu akan memakan mangsa setiap malam!"

    let intro = `Halo ${peran} ${nama}, `

    switch(peran.toLowerCase()) { //pengecekan string case sensitive, jadi ubah string ke upper / lower case.
        case 'penyihir':
            intro+= penyihir
            break
        case 'guard':
            intro+= guard
            break
        case 'werewolf' :
            intro+= wereWolf
            break
        default: //peran tidak sesuai
            return console.log(`\nPeran tidak ditemukan!`)
    }
    console.log(`===========================================`)
    console.log(`Selamat datang di Dunia Werewolf, ${nama}\n`)
    console.log(`${intro}`)
}

initWereWolf()

// ==============================================

async function printTanggal() {
    console.log(`===========================================\n`)
    console.log(`Tugas 2 - Conditional (Switch case) `)
    var tanggal = await questions('tanggal : (1 - 31)\n')
    var bulan = await questions('bulan : (1 - 12)\n')
    var tahun = await questions('tahun : (1900 - 2200)\n' )

    cekInputanTanggal(tanggal,bulan,tahun)

    readLineInt.close()
}

async function cekInputanTanggal(tanggal, bulan, tahun) {
    const tahunChecked = await cekTahun(tahun)
    const bulanChecked = await cekBulan(bulan)
    const tanggalChecked = await cekTanggal(tanggal)

    console.log(`\n${tanggalChecked} ${bulanChecked} ${tahunChecked}`)
}

function cekTahun(tahun = '') {
    switch(parseInt(tahun)) {
        case ((tahun >= 1900 && tahun <= 2200) ? parseInt(tahun) : '') :
            break
        case '' :
        default : 
            tahun = `| Tahun harus berisi angka antara 1900 - 2200! (Anda menginputkan = ${tahun})`
    }

    return promiseData(tahun)
}

function cekBulan(bulan = '') {
    var namaBulan

    switch(parseInt(bulan)) {
        case 1 :
            namaBulan = "Januari"
            break
        case 2 : 
            namaBulan = "Februari"
            break
        case 3 : 
            namaBulan = "Maret"
            break
        case 4 : 
            namaBulan = "April"
            break
        case 5 : 
            namaBulan = "Mei"
            break
        case 6 : 
            namaBulan = "Juni"
            break
        case 7 : 
            namaBulan = "Juli"
            break
        case 8 : 
            namaBulan = "Agustus"
            break
        case 9 : 
            namaBulan = "September"
            break
        case 10 : 
            namaBulan = "Oktober"
            break
        case 11 : 
            namaBulan = "November"
            break
        case 12 : 
            namaBulan = "Desember"
            break
        default :
            namaBulan = `| Bulan Harus berisi angka antara 1 - 12! (Anda menginputkan = ${bulan}) |`
            break
    }

    return promiseData(namaBulan)
}

function cekTanggal(tanggal = '') {
    switch(parseInt(tanggal)) {
        case ((tanggal >= 1 && tanggal <= 31) ? parseInt(tanggal) : '') :
            break
        case '' :
        default :
            tanggal = `Tanggal Harus berisi angka antara 1 - 31! (Anda menginputkan = ${tanggal}) |`
    }

    return promiseData(tanggal)
}

let promiseData = (data) => {
    return new Promise((resolve) => {
        resolve(data)
    })
}