console.log("Soal No. 1 (Array to Object) ")

function arrayToObject(arr) {
    // Code di sini 
    var temp = ''
    var tempObj = []
    var age = ''
    const tahunSekarang = parseInt( new Date().getFullYear() )

    for(i = 0; i < arr.length; i++) {
        if( arr[i][3] > tahunSekarang || !arr[i][3] ) {
            age = 'Invalid Birth Year'
        } else {
            age = tahunSekarang - parseInt( arr[i][3] ) 
        }

        tempObj[i] = {
            'firstName' : arr[i][0],
            'lastName' : arr[i][1],
            'gender' : arr[i][2],
            'age' : age
        } 

        temp += `${i + 1}. ${tempObj[i].firstName} ${tempObj[i].lastName} : {
            firstName : "${tempObj[i].firstName}",
            lastName : "${tempObj[i].lastName}",
            gender : "${tempObj[i].gender}",
            age : ${tempObj[i].age}
        }\n`

        // temp+= `${i + 1}. ${tempObj[i].firstName} ${tempObj[i].lastName} : ${JSON.stringify(tempObj[i])}`
    }

    return temp
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
console.log( arrayToObject(people) ) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
console.log( arrayToObject(people2) ) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
console.log( arrayToObject([]) )// ""

console.log('=====================================\n')
console.log("Soal No. 2 (Shopping Time) ")

function shoppingTime(memberId, money) {
    // you can only write your code here!

    //jika tidak ada memberID
    if(!memberId) return "Mohon maaf, toko X hanya berlaku untuk member saja"

    //jika ada lanjutkan
    var transactionData
    var possibleItem = '' // item yang bisa dibeli

    var boughtItem = [] // item yang dibeli
    
    var changeMoney = ''
    var totalCost = 0


    const barang = [
       { namaBarang : "Sepatu Stacattu", harga : 1500000 },
       { namaBarang : "Baju Zoro", harga : 500000 },
       { namaBarang : "Baju H&N", harga: 250000 },
       { namaBarang : "Sweater Uniklooh", harga : 175000},
       { namaBarang : "Casing Handphone", harga : 50000}
    ]
    possibleItem = barang.filter((e) => { return ( e.harga <= money ) ? true : false })
    //jika tidak ada yang bisa dibeli, maka uang tidak cukup
    if(possibleItem.length == 0) return "Mohon maaf, uang tidak cukup"

    for(i = 0; i < possibleItem.length; i++) {
        if( totalCost + possibleItem[i].harga <= money ) {
            totalCost+= possibleItem[i].harga
            boughtItem[i] = possibleItem[i].namaBarang
        } else {
            break
        }
    }
    changeMoney = money - totalCost

    transactionData = { memberId: memberId,
    money: money,
    listPurchased: boughtItem,
    changeMoney: changeMoney }
    

    return transactionData
}
   
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log('=====================================\n')
console.log("Soal No. 3 (Naik Angkot) ")

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    var dataPerjalanan = []
    var biaya = 2000
    var jarak = ''
    for(i = 0; i < arrPenumpang.length; i++) {
        jarak = rute.indexOf( arrPenumpang[i][1] ) - rute.indexOf( arrPenumpang[i][2] )
        
        dataPerjalanan[i] = {
            penumpang: arrPenumpang[i][0],
            naikDari: arrPenumpang[i][1],
            tujuan: arrPenumpang[i][2],
            bayar: biaya * Math.abs(jarak)
        }
    }



    return dataPerjalanan
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]