/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 * 
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 * 
 * Selamat mengerjakan
*/

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
  // Code disini
  constructor(points) {
    this._points = points
    this._subject
    this._email
  }

  get points() {
    return this._points
  }

  set points(val) {
    this._points = val
  }

  set subject(val) {
    this._subject = val 
  }

  set email(val) { 
    this._email = val
  }

  average() {
    if( isNaN(parseInt(this._points)) ) return 'Not a number!'

    if(Array.isArray(this._points)) {
      return this._points.reduce((a, b) => a + b ) / this._points.length
    }
    return this._points
  }

}

/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ["email", "quiz-1", "quiz-2", "quiz-3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
  // code kamu di sini
  var subjectIndex =  data[0].indexOf(subject)
  if(subjectIndex == -1) return 'subject tidak ditemukan!'

  var output = []
  // var output = []
  var temp = new Score()
  for(i  = 1; i < data.length; i++) {
    // output.push({
    //   email : data[i][0],
    //   subject,
    //   points: data[i][subjectIndex]
    // })

    output.push({
      email: temp.email = data[i][0],
      subject: temp.subject = subject,
      points : temp.points = data[i][subjectIndex]
    })
  }

  return output
}

// TEST CASE
console.log(viewScores(data, "quiz-1"))
console.log(viewScores(data, "quiz-2"))
console.log(viewScores(data, "quiz-3"))

/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

function recapScores(data) {
  // code kamu di sini
  var score = new Score()
  let temp = []
  var Predikat = 'participant'

  for(i = 1; i < data.length; i++) {
    var dup = [...data[i]] // duplicate array
    dup.shift() 
    score.points = dup

    if( score.average() > 70 ) Predikat = 'participant'
    if( score.average() > 80 ) Predikat = 'graduate'
    if( score.average() > 90 ) Predikat = 'honour'

    temp.push({
      email : score.email = data[i][0],
      'Rata-rata' : Math.round(score.average() * 10) / 10,
      Predikat
    })
  }

  var output = ''
  for(i=0 ; i < temp.length; i++) {
    output += `\n${i+1}. ${JSON.stringify(temp[i])} \n`
  } 

  return output
}

console.log(recapScores(data));
