let teriak = (teriakan = `halo sanbers!`) => teriakan

console.log(`\nNo. 1`)
console.log(teriak())
console.log(`===========================================\n`)

function kalikan(angka1, angka2) {
    return angka1 * angka2
}
var num1 = 12
var num2 = 4
var hasilKali = kalikan(num1, num2)

console.log(`\nNo. 2`)
console.log(hasilKali)
console.log(`===========================================\n`)


let introduce = (nama, umur, alamat, hobi) => {
    return `Halo nama saya ${nama}, umur saya ${umur} tahun, alamat saya di ${alamat}, dan saya punya hobby yaitu ${hobi}!`
}

var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby)

console.log(`\nNo. 3`)
console.log(perkenalan)
console.log(`===========================================\n`)
