
// const readline = require('readline')

// const readLineInt = readline.createInterface({
//     input: process.stdin,
//     output: process.stdout
// })

// let questions = question => {
//     return new Promise((resolve) => {
//         readLineInt.question(question, answer => {
//             resolve(answer)
//         })
//     })
// }

// ============================================= 
console.log(`\nNo. 1 - Looping (while)`)
console.log(`===========================================\n`)


console.log(`\nLOOPING PERTAMA\n`)

let whileLength = 20
let whileCounter = 0

while(whileCounter < whileLength) {
    whileCounter += 2
    console.log(`${whileCounter} - I love coding`)
}

//at this line, counter == 20, no need to re initialize
console.log('\nLOOPING KEDUA\n')

while(whileCounter > 0) {
    console.log(`${whileCounter} - I will become a mobile developer`)
    whileCounter -= 2
}


console.log(`\nNo. 2 - Looping (for)`)
console.log(`===========================================\n`)


let forLength = 20 //banyak loop
    
for(let forCounter = 1; forCounter <= forLength; forCounter++) {
    //genap
    if((forCounter % 2) == 0 ) { 
        console.log(`${forCounter} - Berkualitas`)
        continue
    }
    //kelipatan 3 dan bukan genap
    if((forCounter % 3) == 0 && (forCounter % 2) != 0) { 
        console.log(`${forCounter} - I Love Coding`)
        continue
    }
    
    console.log(`${forCounter} - santai`) 
    
}


console.log(`\nNo. 3 - Looping ( Membuat Persegi Panjang # )`)
console.log(`===========================================\n`)

let kolom1 = 8
let baris1 = 4

for(var i = 1; i<=baris1; i++ ) {
    var temp = ''
    for(var j = 1; j <= kolom1; j++) {
        temp += '#'
    }
    console.log(`${temp}`)
}

console.log(`\nNo. 4 - Looping ( Membuat Tangga )`)
console.log(`===========================================\n`)

let tinggi = 7

for(var i = 1; i<=tinggi; i++ ) {
    var temp = ''
    for(var j = 1; j <= i; j++) {
        temp += '#'
    }
    console.log(`${temp}`)
}

console.log(`\nNo. 5 - Looping ( Membuat Papan Catur )`)
console.log(`===========================================\n`)

let panjang = 8
let lebar = 8

for(var i = 1; i<=lebar; i++ ) {
    var temp = ''
    if((i % 2) != 0) {

        for(var j = 1; j <= panjang; j++) {
            if( (j % 2 ) != 0 ) {
                temp += ' '
                continue
            } 
            temp += '#'
        }
    } else {
        for(var j = 1; j <= panjang; j++) {
            if( (j % 2 ) == 0 ) {
                temp += ' '
                continue
            } 
            temp += '#'
        }
    }

    console.log(`${temp}`)
}