console.log("1. Animal Class")
class Animal {
    // Code class di sini
    constructor(name) {
        this._name = name
        this._legs = 4
        this._cold_blooded = false
    }

    get name() {
        return this._name
    }
    get legs() {
        return this._legs
    }
    get cold_blooded() {
        return this._cold_blooded
    }
}

var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

class Ape extends Animal {
    constructor(name) {
        super(name)
        this._legs = 2
    }
    yell() {
        return "Auooo"
    }
}

class Frog extends Animal {
    constructor(name) {
        super(name)
    }
    jump() {
        return "hop hop" 
    }

}

console.log("\n")

var sungokong = new Ape("kera sakti")
console.log(sungokong.yell()) // "Auooo"
// console.log(sungokong.legs)

var kodok = new Frog("buduk")
console.log(kodok.jump()) // "hop hop"

//================================================
console.log("2. Function to Class")

// function Clock({ template }) {
  
//   var timer;

//   function render() {
//     var date = new Date();

//     var hours = date.getHours();
//     if (hours < 10) hours = '0' + hours;

//     var mins = date.getMinutes();
//     if (mins < 10) mins = '0' + mins;

//     var secs = date.getSeconds();
//     if (secs < 10) secs = '0' + secs;

//     var output = template
//       .replace('h', hours)
//       .replace('m', mins)
//       .replace('s', secs);

//     console.log(output);
//   }

//   this.stop = function() {
//     clearInterval(timer);
//   };

//   this.start = function() {
//     render();
//     timer = setInterval(render, 1000);
//   };

// }

// var clock = new Clock({template: 'h:m:s'});
// clock.start(); 

class Clock {
    // Code di sini
    constructor({ template }) {
        this._template = template
        this._timer
    }

    render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
    
        var output =  this._template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);
        
        console.log(output)
    }
 
    stop() {
        clearInterval(timer);
    }
    start() {
        this.render();
        this._timer = setInterval( () => { this.render() }, 1000);
    }
}

var clock = new Clock( {template: 'h:m:s'} );
clock.start(); 