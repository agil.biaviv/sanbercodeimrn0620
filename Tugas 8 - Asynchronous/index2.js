console.log("Soal No. 2 (Promise Baca Buku)\n")

var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
let count = 0
let rejected = false

let baca = (waktu) => {
    if( rejected ) return 

    readBooksPromise(waktu, books[count])
        .then(resolve => { 
            baca(resolve)
        })
        .catch(reject => { 
            rejected = true
        })
    
    count+=1
}

baca(10000)