// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
]
 
// Tulis code untuk memanggil function readBooks di sini
let count = 0
let time = 10000

let bacaLagi = (waktu) => { 
    count+=1
    if(count > books.length - 1 ) return 
    readBooks(waktu, books[count], bacaLagi)
} 
console.log("Soal No. 1 (Callback Baca Buku)\n")
readBooks(time, books[count], bacaLagi)
